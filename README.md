# LIS4381 FALL15 A1
## Cuong Huynh

==============================================

## AMPPS Installation Screenshot
[![LinkedIn](images/ssAMPPS.png)](images/ssAMPPS.png)

==============================================

## Git command descriptions

1. git init - creates a new local repository
2. git status - lists the files that have changed and also the files that need to add or commit
3. git add - adds one or more files to the index depending on parameters (git add *, git add <filename>
4. git commit - using "git commit" with "-m" command commits changes to head (not yet remote), using "git commit" with "-a" command commits files added with "git add" or any files changes since
5. git push - sends the changes to the master branch of the remote repository
6. git pull - fetches and merges changes on the remote server to your working directory
7. git merge - merges a different branch into your active branch

==============================================

## Links
[Bitbucket Repo](https://cnh14e@bitbucket.org/cnh14e/lis4381_fall15_a1.git)  

[Bitbucket Tutorial 1](https://bitbucket.org/cnh14e/tutorials.git.bitbucket.org)

[Bitbucket Tutorial 2](https://cnh14e@bitbucket.org/cnh14e/bitbucketstationlocations.git)

[Link to default main page](http://CuongNHuynh.com)